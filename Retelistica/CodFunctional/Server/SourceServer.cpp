#include <iostream>
#include <winsock2.h>

#pragma comment(lib, "ws2_32.lib")

using namespace std;

int main()
{
	WSADATA WSAData;
	SOCKET server, client;
	SOCKADDR_IN serverAddr, clientAddr;
	int port = 5555;
	int optionZero = 0;
	int optionTwo = 2;
	const int bufferSize = 1024;

	WSAStartup(MAKEWORD(optionTwo, optionZero), &WSAData); //se initializeaza WSA (legat de windows)
	server = socket(AF_INET, SOCK_STREAM, 0); //creeaza socket-ul server (listener-ul) 

	if (server == INVALID_SOCKET) {
		throw "Error at socket creation"; //aici ar putea fi un throw exception
		return 1;
	}

	serverAddr.sin_addr.s_addr = INADDR_ANY; //poate sa primeasca clienti avand orice IP
	serverAddr.sin_family = AF_INET; // protocol TCP IP
	serverAddr.sin_port = htons(port); // port

	// bind=cand pune proprietatile pe care le-a pus in serverAddr la socket-ul server pe care l-a creat
	try
	{
		bind(server, (SOCKADDR *)&serverAddr, sizeof(serverAddr));
	}
	catch (...)
	{
		throw "Error at binding";
		return 1;
	}

	/*
	if (bind(server, (SOCKADDR *)&serverAddr, sizeof(serverAddr)) == SOCKET_ERROR) {
		throw "Error at binding"; //aici ar putea fi un throw exception
		return 1;
	}
	*/

	listen(server, optionZero); // listen- asculta si asteapta sa se conecteze un client

	cout << "Listening for incoming connections..." << endl;

	char buffer[bufferSize]; // mesajul pe care il primesti de la client
	int clientAddrSize = sizeof(clientAddr); //marimea mesajului

	client = accept(server, (SOCKADDR *)&clientAddr, &clientAddrSize); // accept creeaza socket-ul prin care comunica cu clientul 
	if (client == INVALID_SOCKET) {
		throw "Error at client acception"; //throw ar merge aici
		return 1;
	}

	cout << "Client connected!" << endl;
	recv(client, buffer, sizeof(buffer), optionZero); // cu recv primesc mesajul
	cout << "Client says: " << buffer << endl;
	strcpy_s(buffer, "hi");
	send(client, buffer, sizeof(buffer), optionZero); //send- server-ul trimite la client un mesaj

	//closesocket(client); // inchide socket-ul client
	//closesocket(server);
	cout << "Client disconnected." << endl;
	system("pause");
	return 0;
}