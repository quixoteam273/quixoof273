#pragma once
#include <iostream>
#include <string>
#include <vector>

#include <winsock2.h>
#include <Windows.h>
#include "NetworkData.h"

class NetworkServices
{
public:
	static int sendMessage(SOCKET curSocket, char* message, int messageSize);
	static int receiveMessage(SOCKET curSocket, char* buffer, int bufSize);
};