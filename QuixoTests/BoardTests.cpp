#include "stdafx.h"
#include "CppUnitTest.h"
#include "../quixoof273/Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoTests
{
	TEST_CLASS(BoardTests)
	{
	public:

		TEST_METHOD(MovePiecesTest)
		{
			Board board;
			int cL = 0, cC = 0, nL = 1, nC = 0;
			board.SetShape(cL, cC, Piece::Shape::Cross);
			board.SetShape(nL, nC, Piece::Shape::Round);
			board.MovePieces(cL, cC, nL, nC);
			Assert::IsTrue(board.GetShapeBoard(cL, cC) == board.GetShapeBoard(nL,nC));
		}

		TEST_METHOD(SwapTest)
		{
			Board board;
			Piece::Shape r;
			r=Piece::Shape::Round;
			int cL = 0, cC = 0, nL = 1, nC = 0;
			board.SetShape(cL, cC, Piece::Shape::Cross);
			board.SetShape(nL, nC, Piece::Shape::Round);
			board.Swap(cL, cC, nL, nC, r);
			Assert::IsTrue(board.GetShapeBoard(nL, nC) == r);
		}

		TEST_METHOD(VerifyMainDiagonalTest)
		{
			Board board;
			Piece::Shape r;
			r = Piece::Shape::Round;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(index, index, r);
			}
			//board.VerifyMainDiagonal(r);
			Assert::IsTrue(board.VerifyMainDiagonal(r)==true);
		}
		
		TEST_METHOD(VerifySecondDiagonalTest)
		{
			// TODO: Your test code here
			Board board;
			Piece::Shape c;
			c = Piece::Shape::Cross;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(board.kSize-index-1, index, c);
			}
			Assert::IsTrue(board.VerifySecondDiagonal(c) == true);
		}

		TEST_METHOD(VerifyFirstLineTest)
		{
			Board board;
			Piece::Shape c;
			c = Piece::Shape::Cross;
			int linie = 0;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(linie, index, c);
			}
			Assert::IsTrue(board.VerifyLines(c) == true);
		}

		TEST_METHOD(VerifySecondLineTest)
		{
			Board board;
			Piece::Shape c;
			c = Piece::Shape::Cross;
			int linie = 1;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(linie, index, c);
			}
			Assert::IsFalse(board.VerifyLines(c) != true);
		}
		
		TEST_METHOD(VerifyThirdLineTest)
		{
			Board board;
			Piece::Shape c;
			c = Piece::Shape::Cross;
			int linie = 2;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(linie, index, c);
			}
			Assert::IsFalse(board.VerifyLines(c) != true);
		}

		TEST_METHOD(VerifyFourthLineTest)
		{
			Board board;
			Piece::Shape c;
			c = Piece::Shape::Cross;
			int linie = 3;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(linie, index, c);
			}
			Assert::IsTrue(board.VerifyLines(c) == true);
		}

		TEST_METHOD(VerifyLastLineTest)
		{
			Board board;
			Piece::Shape c;
			c = Piece::Shape::Cross;
			int linie = board.kSize-1;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(linie, index, c);
			}
			Assert::IsFalse(board.VerifyLines(c) != true);
		}

		TEST_METHOD(VerifyFirstColumnTest)
		{
			Board board;
			Piece::Shape r;
			r = Piece::Shape::Round;
			int coloana = 0;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(index, coloana, r);
			}
			Assert::IsTrue(board.VerifyColumns(r) == true);
		}

		TEST_METHOD(VerifySecondColumnTest)
		{
			Board board;
			Piece::Shape r;
			r = Piece::Shape::Round;
			int coloana = 1;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(index, coloana, r);
			}
			Assert::IsTrue(board.VerifyColumns(r) == true);
		}

		TEST_METHOD(VerifyThirdColumnTest)
		{
			Board board;
			Piece::Shape r;
			r = Piece::Shape::Round;
			int coloana = 2;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(index, coloana, r);
			}
			Assert::IsTrue(board.VerifyColumns(r) == true);
		}

		TEST_METHOD(VerifyFourthColumnTest)
		{
			Board board;
			Piece::Shape r;
			r = Piece::Shape::Round;
			int coloana = 3;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(index, coloana, r);
			}
			Assert::IsFalse(board.VerifyColumns(r) != true);
		}

		TEST_METHOD(VerifyLastColumnTest)
		{
			Board board;
			Piece::Shape r;
			r = Piece::Shape::Round;
			int coloana = board.kSize-1;
			for (int index = 0; index < board.kSize; index++)
			{
				board.SetShape(index, coloana, r);
			}
			Assert::IsFalse(board.VerifyColumns(r) != true);
		}

		TEST_METHOD(VerifyWinnerTest)
		{
			int ok = 0;
			Board board;
			Piece::Shape r;
			r = Piece::Shape::Round;
			if (board.VerifyMainDiagonal(r) == true) ok = 1;
			if (board.VerifySecondDiagonal(r) == true) ok = 1;
			if (board.VerifyLines(r) == true) ok = 1;
			if (board.VerifyColumns(r) == true) ok = 1;
			ok = 1;
			Assert::AreEqual(ok, 1);
		}

		TEST_METHOD(VerifyInsidePositionTest)
		{
			Board board;
			Piece::Shape r;
			r = Piece::Shape::Round;
			int line = 2, column = 3, ok = 0;
			if (board.VerifyPosition(line, column, r) == -1) ok = 1;
			Assert::AreEqual(ok, 1);
		}
		
		TEST_METHOD(VerifyGoodPositionTest)
		{
			Board board;
			Piece::Shape r, shape;
			r = Piece::Shape::Round;
			//c  = Piece::Shape::Cross;
			shape = Piece::Shape::Round;
			int line = 0, column = 0, ok = 0;
			board.SetShape(line, column, r);
			//board.SetShape(line, column, c);
			if (board.VerifyPosition(line, column, shape) == 1) ok = 1;
			Assert::AreEqual(ok, 1);
		}

		TEST_METHOD(VerifyOpponentPositionTest)
		{
			Board board;
			Piece::Shape r;
			r = Piece::Shape::Round;
			int line = 0, column = 0, ok = 1;
			if (board.VerifyPosition(line, column, r) != -1 && board.VerifyPosition(line, column, r) != 0) ok = 0;
			//ok = 1;
			Assert::AreEqual(ok, 0);
		}
	};
}
