#include "stdafx.h"
#include "CppUnitTest.h"
#include "../quixoof273/Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoTests
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(CopyConstructor)
		{
			std::string name="name";
			Player player1(name);
			Player player2(player1);
			Assert::IsTrue(player2.GetName() == name);
		}

		TEST_METHOD(OutputOperator)
		{
			std::string name=" ";
			Player player(name);
			std::stringstream out;
			out << player;
			Assert::IsTrue(out.str() == name);
		}	

		/*TEST_METHOD(SelectPiece)
		{
			Board board;
			Piece::Shape piece;
			int L = 0, C = 0, wantedColumn = 2, wantedLine = 1;
			board.VerifyPosition(L, C, piece);
			board.MovePieces(L, wantedColumn, L + 1, wantedColumn);		
			board.SetShape(wantedLine, wantedColumn, piece);
			Assert::IsTrue();
			
		}*/
	};
}
