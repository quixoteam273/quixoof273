#include "stdafx.h"
#include "CppUnitTest.h"
//#include <sstream>
#include "../quixoof273/Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoTests
{
	TEST_CLASS(PieceTests)
	{
	public:

		TEST_METHOD(ConstructorWithOneParametere)
		{
			// TODO: Your test code here
			Piece piece(Piece::Shape::Round);
			Assert::IsTrue(piece.GetShape() == Piece::Shape::Round);
		}

		TEST_METHOD(CopyConstructor)
		{
			Piece piece1(Piece::Shape::Cross);
			Piece piece2(piece1);
			Assert::IsTrue(piece2.GetShape() == Piece::Shape::Cross);
		}

		TEST_METHOD(OutputOperator)
		{
			Piece piece(Piece::Shape::Round);
			std::stringstream out;
			out << piece;
			Assert::IsTrue(out.str()=="O");
		}

		TEST_METHOD(ComparisonOperator)
		{
			Piece piece1(Piece::Shape::Cross);
			Piece piece2(Piece::Shape::Cross);
			Assert::IsTrue(piece1.GetShape() == piece2.GetShape());
		}

		TEST_METHOD(NotEqualOperator)
		{
			Piece piece1(Piece::Shape::Round);
			Piece piece2(Piece::Shape::Cross);
			Assert::IsTrue(piece1.GetShape() != piece2.GetShape());
		}

		TEST_METHOD(EqualOperator)
		{
			
			Piece piece1(Piece::Shape::Round);
			Piece piece2;
			piece2 = piece1;
			//Assert::AreEqual(static_cast<Piece::Shape>(piece1.GetShape()), static_cast<Piece::Shape>(piece2.GetShape()));
			Assert::IsTrue(piece1.GetShape() == piece2.GetShape());
		}
	};
}
