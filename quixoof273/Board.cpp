#include "Board.h"

Board::Board()
{
	//Empty
}

Board::~Board()
{
	//Empty
}

Piece::Shape Board::GetShapeBoard(int currentLine, int currentColumn)
{
   return m_pieces[currentLine][currentColumn].GetShape();
}

void Board::MovePieces(int currentLine, int currentColumn, int newLine, int newColumn)
{
	m_pieces[currentLine][currentColumn] = m_pieces[newLine][newColumn];
}

void Board::SetShape(int currentLine, int currentColumn, Piece::Shape shape)
{
	m_pieces[currentLine][currentColumn] = shape;
}

void Board::Swap(int currentLine, int currentColumn, int newLine, int newColumn, Piece::Shape shape)
{
	MovePieces(currentLine, currentColumn, newLine, newColumn);
	SetShape(newLine, newColumn, shape);
}

bool Board::VerifyMainDiagonal(Piece::Shape shape) const
{
	int ok;
	for (int index = 0; index < kSize - 1; index++)
		if (m_pieces[index][index] == shape && m_pieces[index][index] == m_pieces[index + 1][index + 1]) ok = 1;
		else {
			ok = 0;
			break;
		}
	if (ok == 1) return true;
	return false;
}

bool Board::VerifySecondDiagonal(Piece::Shape shape) const
{
	int ok;
	for (int index = 0; index < kSize - 1; index++)
		if (m_pieces[kSize - index - 1][index] == shape && m_pieces[kSize - index - 1][index] == m_pieces[kSize - index - 2][index + 1]) ok = 1;
		else {
			ok = 0;
			break;
		}
	if (ok == 1) return true;
	return false;
}

bool Board::VerifyLines(Piece::Shape shape) const
{
	for (int indexL = 0; indexL < kSize; indexL++)
	{
		int ok;
		for (int indexC = 0; indexC < kSize - 1; indexC++)
			if (m_pieces[indexL][indexC] == shape && m_pieces[indexL][indexC] == m_pieces[indexL][indexC + 1])
				ok = 1;
			else {
				ok = 0;
				break;
			}
		if (ok == 1) return true;
	}
	return false;
}

bool Board::VerifyColumns(Piece::Shape shape) const
{
	for (int indexC = 0; indexC < kSize; indexC++)
	{
		int ok;
		for (int indexL = 0; indexL < kSize - 1; indexL++)
			if (m_pieces[indexL][indexC] == m_pieces[indexL + 1][indexC] && m_pieces[indexL][indexC] == shape)
				ok = 1;
			else
			{
				ok = 0;
				break;
			}
		if (ok == 1) return true;
	}
	return false;
}


bool Board::VerifyWinner(Piece::Shape shape) const
{
	if (VerifyMainDiagonal(shape) == true) return true;
	if (VerifySecondDiagonal(shape) == true) return true;
	if (VerifyLines(shape) == true) return true;
	if (VerifyColumns(shape) == true) return true;
	return false;
}

int Board::VerifyPosition(int line, int column, Piece::Shape shape) const
{
	if ((line >= 1 && line < kSize - 1) && (column >= 1 && column < kSize - 1)) return -1;

	if (m_pieces[line][column].GetShape() == Piece::Shape::Round && m_pieces[line][column].GetShape() == shape)
		return 1;
	if (m_pieces[line][column].GetShape() == Piece::Shape::Cross && m_pieces[line][column].GetShape() == shape)
		return 1;
	if (m_pieces[line][column].GetShape() == Piece::Shape::Unpicked)
		return 1;

	return 0;
}

std::ostream & operator << (std::ostream & os, const Board& board)
{
	for (int line = 0; line < board.kSize; ++line)
	{
		for (int column = 0; column < board.kSize; ++column)
			os << board.m_pieces[line][column] << "     ";
		os << std::endl<<std::endl;
	}
	return os;
}