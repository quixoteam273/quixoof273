#include "Piece.h"

Piece::Piece() :
	Piece(Shape::Unpicked)
{
	//Empty
}

Piece::Piece(Shape shape)
{
	m_shape = shape;
	static_assert(sizeof(*this) <= 1, "This class should be 1 byte in size");
}

Piece::Piece(const Piece& other)
{
	*this = other;
}

Piece::~Piece()
{
	//Empty
}

Piece::Shape Piece::GetShape() const
{
	return m_shape;
}

bool Piece::operator==(const Piece& other) const
{
	if (this->GetShape() == other.GetShape())
		return true;
	return false;
}

bool Piece::operator!=(const Piece& other) const
{
	if (this->GetShape() == other.GetShape())
		return false;
	return true;
}

Piece & Piece::operator=(const Piece& other)
{
	m_shape = other.m_shape;
	return *this;
}

std::ostream & operator << (std::ostream& os, const Piece& piece)
{
	int aux;
	aux = static_cast<int>(piece.m_shape);
	if (aux == 0)
		os << '_';
	else
		if (aux == 1)
			os << 'X';
		else
			os << 'O';
	return os;
}