#pragma once
#include <string>
#include "Board.h"

class Player
{
public:
	Player();
	Player(const std::string& name);
	~Player();

	std::string	GetName();
	void SetName(const std::string& name);
	void SelectPiece(int line, int column, Board& board, Piece::Shape aux) const;

	friend std::ostream& operator << (std::ostream& os, const Player& player);

private:
	std::string m_name;
};