#include <iostream>
#include "QuixoGame.h"
#include "../Logging/Logging.h"
#include <fstream>

int main()
{
	QuixoGame game;

	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("Started Application RUN...", Logger::Level::Info);

	//logger.log("Error in Application RUN", Logger::Level::Error);
	//logger.log("Warning in Application RUN", Logger::Level::Warning);
	game.Run();

	return 0;
}