#include "QuixoGame.h"
#include <stdlib.h>
#include "../Logging/Logging.h"
#include <fstream>

QuixoGame::QuixoGame()
{
	//Empty
}

QuixoGame::~QuixoGame()
{
	//Empty
}

void QuixoGame::Run()
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("Started Application RUN in QuixoGame class", Logger::Level::Info);

	std::string name;
	char opt;
	auto round = Piece::Shape::Round;
	auto cross = Piece::Shape::Cross;
	Player player1;
	Player player2;
	Board board;
	int line, l;
	int column, c;


	system("Color F0");
	std::cout << "___QUIXO GAME___ \n";
	std::cout << "\n\n";

	std::cout << "Insert the name of the player with shape 'X': ";
	std::cin >> name;
	player1.SetName(name);

	std::cout << "Insert the name of the player with shape 'O': ";
	std::cin >> name;
	player2.SetName(name);
	system("CLS");

	std::cout << board << "\n\n";
	std::cout << player1.GetName() << ": is the player one with 'X' \n" << player2.GetName()
		<< ": is the player two with 'O' \n\n\n";
	std::cout << "GOOD LUCK! \n\n";

	while (true)
	{
		std::cout << player1.GetName() << " with 'X', it's your turn to pick a piece! \n";
		std::cout << "Line: "; std::cin >> l; line = l - 1;
		std::cout << "Column: "; std::cin >> c; column = c - 1;

		std::ofstream of("syslog.log", std::ios::app);
		Logger logger(of);
		logger.log("ERROR for VerifyPosition function -> Choose another piece from board", Logger::Level::Info);
		if (l <= 0 || c <=0 || l>=5 || c>=5)  logger.log("ERROR for VerifyPosition function -> Outside position", Logger::Level::Error);
		//logger.log("ERROR for VerifyPosition function -> Choose another piece from board", Logger::Level::Error);

		if (board.VerifyPosition(line, column, cross) == 0 || board.VerifyPosition(line, column, cross) == -1)
		{
			while (board.VerifyPosition(line, column, cross) == 0 || board.VerifyPosition(line, column, cross) == -1)
				if (board.VerifyPosition(line, column, cross) == 0)
				{
					std::cout << "ERROR!!! You tried to move your opponent's piece. Choose other piece! \n" << std::endl;
					std::cout << "Line: "; std::cin >> l; line = l - 1;
					std::cout << "Column: "; std::cin >> c; column = c - 1;
				}
				else
				{
					std::cout << "ERROR!!! You tried to move a piece from the inside. Choose other piece! \n" << std::endl;
					std::cout << "Line: "; std::cin >> l; line = l - 1;
					std::cout << "Column: "; std::cin >> c; column = c - 1;
				}

			player1.SelectPiece(line, column, board, cross);
		}
		else
			player1.SelectPiece(line, column, board, cross);
		system("CLS");

		std::cout << board << std::endl;

		if (board.VerifyWinner(cross) == true && board.VerifyWinner(round) == true)	
		{

			std::ofstream of("syslog.log", std::ios::app);
			Logger logger(of);
			logger.log("Player 2 is the winner even if Player 1 won first the game because by his last move won the second Player!", Logger::Level::Info);

			std::cout << player2.GetName() << " is the winner! \n" << std::endl;
			break;
		}
		else
			if (board.VerifyWinner(cross) == true)
			{
				std::cout << player1.GetName() << " is the winner! \n" << std::endl;
				break;
			}
			else
				if (board.VerifyWinner(round) == true)
				{
					std::cout << player2.GetName() << " is the winner! \n" << std::endl;
					break;
				}

		std::cout << player2.GetName() << " with 'O', it's your turn to pick a piece! \n";
		std::cout << "Line: "; std::cin >> l; line = l - 1;
		std::cout << "Column: "; std::cin >> c; column = c - 1;

		if (board.VerifyPosition(line, column,  round) == 0 || board.VerifyPosition(line, column, round) == -1)
		{
			while (board.VerifyPosition(line, column, round) == 0 || board.VerifyPosition(line, column, round) == -1)
				if (board.VerifyPosition(line, column, round) == 0)
				{
					std::cout << "ERROR!!! You tried to move your opponent's piece. Choose other piece! \n" << std::endl;
					std::cout << "Line: "; std::cin >> l; line = l - 1;
					std::cout << "Column: "; std::cin >> c; column = c - 1;
				}
				else
				{
					std::cout << "ERROR!!! You tried to move a piece from the inside. Choose other piece! \n" << std::endl;
					std::cout << "Line: "; std::cin >> l; line = l - 1;
					std::cout << "Column: "; std::cin >> c; column = c - 1;
				}

			player2.SelectPiece(line, column, board, round);
		}
		else
			player2.SelectPiece(line, column, board, round);
		system("CLS");

		if (board.VerifyWinner(round) == true && board.VerifyWinner(cross) == true)
		{
			std::cout << player1.GetName() << " is the winner! \n" << std::endl;
			break;
		}
		else
			if (board.VerifyWinner(round) == true)
			{
				std::cout << player2.GetName() << " is the winner! \n" << std::endl;
				break;
			}
			else
				if (board.VerifyWinner(cross) == true)
				{
					std::cout << player1.GetName() << " is the winner! 'n" << std::endl;
					break;
				}

		std::cout << "\n";
		std::cout << board << std::endl;
	}

	//system("CLS");
	std::cout << board << std::endl;
	std::cout << "Do you want to play again? \n";
	std::cout << " Choose 'y' for YES and 'n' for no. \n";
	std::cin >> opt;
	if (opt == 'y')
	{
		std::cout << "\n\n\n\n\n";
		Run();
	}
	else
		std::cout << "Thank you for playing! :) \n";
}