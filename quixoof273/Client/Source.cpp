#include <iostream>
#include <winsock2.h>

#pragma warning(disable:4996)
#pragma comment(lib, "ws2_32.lib")

using namespace std;

int main()
{
	WSADATA WSAData;
	SOCKET server;
	SOCKADDR_IN addr; //addr= informatii despre socket-ul client
	int port = 5555;
	int optionZero = 0;
	int optionTwo = 2;
	const int bufferSize = 1024;

	WSAStartup(MAKEWORD(optionTwo, optionZero), &WSAData);
	server = socket(AF_INET, SOCK_STREAM, optionZero);

	addr.sin_addr.s_addr = inet_addr("25.70.83.25"); // socket-ul client, spre deosebire de socket-ul server poate sa trimita informatia la un singur IP
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);

	try 
	{
		connect(server, (SOCKADDR *)&addr, sizeof(addr));
	}
	catch (...)
	{
		throw "Error at server connection.";
		return 1;
	}

	/*
	if (connect(server, (SOCKADDR *)&addr, sizeof(addr)) == SOCKET_ERROR) //connect- functie cu care se conecteaza la server 
	{
		cout << "Error at server connection";
		return 1;
	}
	*/

	cout << "Connected to server!" << endl;

	char buffer[bufferSize] = { 'h', 'e', 'l', 'l', 'o', '.' }; // inializare measjului pe care il trimiti catre server
	send(server, buffer, sizeof(buffer), optionZero); //send- functie pentru trimitere mesaj
	cout << "Message sent!" << endl;

	char answ[bufferSize]; // mesajul pe care il primesti de la server
	recv(server, answ, sizeof(answ), optionZero); //recv=recive
	cout << answ << endl;

	closesocket(server);
	WSACleanup();
	cout << "Socket closed." << endl << endl;
	system("pause");
	return 0;
}