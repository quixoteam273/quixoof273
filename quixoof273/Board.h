#pragma once
#include "Piece.h"
#include <array>

class Board
{
public:
	static const size_t kSize = 5;

public:
	Board();
	~Board();

	Piece::Shape GetShapeBoard(int currentLine, int currentColumn); 
	void MovePieces(int currentLine, int currentColumn, int newLine, int newColumn);
	void SetShape(int currentLine, int currentColumn, Piece::Shape shape);
	void Swap(int currentLine, int currentColumn, int newLine, int newColumn, Piece::Shape aux);

	bool VerifyMainDiagonal(Piece::Shape shape) const;
	bool VerifySecondDiagonal(Piece::Shape shape) const;
	bool VerifyLines(Piece::Shape shape) const;
	bool VerifyColumns(Piece::Shape shape) const;

	bool VerifyWinner(Piece::Shape shape) const;
	int VerifyPosition(int line, int column, Piece::Shape shape) const;

	friend std::ostream& operator <<(std::ostream& os, const Board& board);

private:
	std::array<std::array<Piece, kSize>, kSize> m_pieces;
};