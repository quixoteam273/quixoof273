#pragma once
#include <iostream>

class Piece
{
public:
	enum class Shape : uint8_t
	{
		Unpicked,
		Cross,
		Round
	};

public:
	Piece();
	Piece(Shape shape);
	Piece(const Piece& other);
	~Piece();

	Shape GetShape() const;

	bool operator==(const Piece& other)const;
	bool operator!=(const Piece& other)const;
	Piece& operator =(const Piece& other);

	friend std::ostream& operator << (std::ostream& os, const Piece& piece);

private:
	Shape m_shape;
};