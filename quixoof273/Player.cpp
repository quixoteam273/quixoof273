#include "Player.h"

Player::Player()
{
	//Empty
}

Player::Player(const std::string & name)
{
	m_name = name;
}

Player::~Player()
{
	//Empty
}

std::string Player::GetName()
{
	return m_name;
}

void Player::SetName(const std::string& name)
{
	m_name = name;
}

void Player::SelectPiece(int line, int column, Board& board, Piece::Shape aux) const
{
	int wantedLine;
	int wantedColumn;

	if (board.VerifyPosition(line, column, aux) == 1)
	{
		if (line == 0 && column == 0)
		{
			std::cout << "Possible moves are (5, 1) and (1, 5). \n" << "Please insert the wanted position! \n";
			std::cout << "Wanted line: "; std::cin >> wantedLine;
			std::cout << "Wanted column: "; std::cin >> wantedColumn;

			if (wantedLine == 5 && wantedColumn == 1)
			{
				for (int i = 0; i < 4; i++)
					board.MovePieces(i, wantedColumn-1, i + 1, wantedColumn-1);			
				board.SetShape(wantedLine-1, wantedColumn-1, aux);
			}
			else
				if (wantedLine == 1 && wantedColumn == 5)
				{
					for (int i = 0; i < 4; i++)
						board.MovePieces(wantedLine-1, i, wantedLine-1, i + 1);			
					board.SetShape(wantedLine-1, wantedColumn-1, aux);
				}
		}

		if (line == 4 && column == 4)
		{
			std::cout << "Possible moves are (1, 5) and (5, 1). \n" << "Please insert the wanted position! \n";
			std::cout << "Wanted line: "; std::cin >> wantedLine;
			std::cout << "Wanted column: "; std::cin >> wantedColumn;

			if (wantedLine == 1 && wantedColumn == 5)
			{
				for (int i = 4; i > 0; i--)
					board.MovePieces(i, wantedColumn-1, i - 1, wantedColumn-1);
				board.SetShape(wantedLine-1, wantedColumn-1, aux);
			}
			else
				if (wantedLine == 5 && wantedColumn == 1)
				{
					for (int i = 4; i > 0; i--)
						board.MovePieces(wantedLine-1, i, wantedLine-1, i - 1);
					board.SetShape(wantedLine-1, wantedColumn-1, aux);
				}
		}

		if (line == 0 && column == 4)
		{
			std::cout << "Possible moves are (1, 1) and (5, 5). \n" << "Please insert the wanted position! \n";
			std::cout << "Wanted line: "; std::cin >> wantedLine;
			std::cout << "Wanted column: "; std::cin >> wantedColumn;

			if (wantedLine == 1 && wantedColumn == 1)
			{
				for (int i = 4; i > 0; i--)
					board.MovePieces(wantedLine-1, i, wantedLine-1, i - 1);
				board.SetShape(wantedLine-1, wantedColumn-1, aux);
			}
			else
				if (wantedLine == 5 && wantedColumn == 5)
				{
					for (int i = 0; i < 4; i++)
						board.MovePieces(i, wantedColumn-1, i + 1, wantedColumn-1);	 
					board.SetShape(wantedLine-1, wantedColumn-1, aux);	
				}
		}

		if (line == 4 && column == 0)
		{
			std::cout << "Possible moves are (5, 5) and (1, 1). \n" << "Please insert the wanted position! \n";
			std::cout << "Wanted line: "; std::cin >> wantedLine;
			std::cout << "Wanted column: "; std::cin >> wantedColumn;

			if (wantedLine == 5 && wantedColumn == 5)
			{
				for (int i = 0; i < 4; i++)
					board.MovePieces(wantedLine-1, i, wantedLine-1, i + 1);
				board.SetShape(wantedLine-1, wantedColumn-1, aux);
			}
			else
				if (wantedLine == 1 && wantedColumn == 1)
				{
					for (int i = 4; i > 0; i--)
						board.MovePieces(i, wantedColumn-1, i - 1, wantedColumn-1);
					board.SetShape(wantedLine-1, wantedColumn-1, aux);
				}
		}

		if (line == 0)
		{
			for (int index = 1; index < board.kSize - 1; index++)
			{
				if (column == index)
				{
					std::cout << "Possible moves are (1, 1), (1, 5) and (5, " <<index+1<< ")." << "\n" 
						<< "Please insert the wanted position! \n";
					std::cout << "Wanted line: "; std::cin >> wantedLine;
					std::cout << "Wanted column: "; std::cin >> wantedColumn;
					if (wantedLine == 1 && wantedColumn == 1)
					{
						for (int i = column; i > 0; i--)
							board.MovePieces(wantedLine - 1, i, wantedLine - 1, i - 1);
						board.SetShape(wantedLine - 1, wantedColumn - 1, aux);
					}
					else
						if (wantedLine == 1 && wantedColumn == 5)
						{
							for (int i = column; i < 4; i++)
								board.MovePieces(wantedLine - 1, i, wantedLine - 1, i + 1);			
							board.SetShape(wantedLine - 1, wantedColumn - 1, aux);
						}
						else
							if (wantedLine == 5 && wantedColumn == column + 1)
							{
								for (int i = 0; i < 4; i++)
									board.MovePieces(i, wantedColumn - 1, i + 1, wantedColumn - 1);			
								board.SetShape(wantedLine - 1, wantedColumn - 1, aux);
							}
				}
			}
		}
		
		if (line == board.kSize-1)
		{
			for (int index = 1; index < board.kSize - 1; index++)
			{
				if (column == index)
				{
					std::cout << "Possible moves are (5, 1), (5, 5) and (1, " << index + 1<<"). \n" 
						<<"Please insert the wanted position! \n";
					std::cout << "Wanted line: "; std::cin >> wantedLine;
					std::cout << "Wanted column: "; std::cin >> wantedColumn;

					if (wantedLine == 5 && wantedColumn == 1)
					{
						for (int i = column; i > 0; i--)
							board.MovePieces(wantedLine-1, i, wantedLine-1, i - 1);
						board.SetShape(wantedLine-1, wantedColumn-1, aux);
					}
					else
						if (wantedLine == 1 && wantedColumn == index+1)
						{
							for (int i = 4; i > 0; i--)
								board.MovePieces(i, wantedColumn-1, i - 1, wantedColumn-1);
							board.SetShape(wantedLine-1, wantedColumn-1, aux);
						}
						else
							if (wantedLine == 5 && wantedColumn == 5)
							{
								for (int i = column; i < 4; i++)
									board.MovePieces(i, wantedColumn-1, i + 1, wantedColumn-1);			
								board.SetShape(wantedLine-1, wantedColumn-1, aux);
							}
				}
			}
		}

		if (column == 0)	
		{
			for (int index = 1; index < board.kSize - 1; index++)
			{
				if (line == index)
				{
					std::cout << "Possible moves are (1, 1), (5, 1) and ("<<index+1<< ", 5). \n "
						<<"Please insert the wanted position! \n";
					std::cout << "Wanted line: "; std::cin >> wantedLine;
					std::cout << "Wanted column: "; std::cin >> wantedColumn;

					if (wantedLine == 1 && wantedColumn == 1)
					{
						for (int i = line; i > 0; i--)
							board.MovePieces(i, column, i - 1, column);
						board.SetShape(wantedLine-1, wantedColumn-1, aux);
					}
					else
						if (wantedLine == 5 && wantedColumn == 1)
						{
							for (int i = line; i < 4; i++)
								board.MovePieces(i, column, i + 1, column);
							board.SetShape(wantedLine-1, wantedColumn-1, aux);
						}
						else
							if (wantedLine == index+1 && wantedColumn == 5)
							{
								for (int i = 0; i < 4; i++)
									board.MovePieces(line, i, line, i + 1);
								board.SetShape(wantedLine-1, wantedColumn-1, aux);
							}
				}
			}
		}

		if (column == 4)
		{
			for(int index=1; index<board.kSize-1; index++)
				if (index == line)
				{
					std::cout << "Possible moves are (1, 5), (5, 5) and ("<< index+1 << ", 1). \n"
						<< "Please insert the wanted position! \n";
					std::cout << "Wanted line: "; std::cin >> wantedLine;
					std::cout << "Wanted column: "; std::cin >> wantedColumn;

					if (wantedLine == 1 && wantedColumn == 5)
					{
						for (int i = line; i > 0; i--)
							board.MovePieces(i, column, i - 1, column);
						board.SetShape(wantedLine-1, wantedColumn-1, aux);
					}
					else
						if (wantedLine == 5 && wantedColumn == 5)
						{
							for (int i = line; i < 4; i++)
								board.MovePieces(i, column, i + 1, column);
							board.SetShape(wantedLine-1, wantedColumn-1, aux);
						}
						else
							if (wantedLine == index+1 && wantedColumn == 1)
							{
								for (int i = column; i > 0; i--)
									board.MovePieces(line, i, line, i - 1);
								board.SetShape(wantedLine-1, wantedColumn-1, aux);
							}
				}
		}
	}
}

std::ostream & operator<<(std::ostream & os, const Player & player)
{
	os << player.m_name;
	return os;
}